import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { io } from "socket.io-client";
import { Observable, Subscriber } from 'rxjs';

const DISH_ENDPOINT = "dish";

@Injectable({
  providedIn: 'root'
})
export class DishesService {

  socket;

  constructor(
    private _http: HttpClient
  ) { 
    this.socket = io(environment.SOCKET_PORT);
  }

  public async getAll() {
    try {
      const result = await this._http.get(environment.API_URL.concat(DISH_ENDPOINT).concat('/all')).toPromise();
      return result;
    } catch (error) {
      return error;
    }
  }

  public async deliverFood(dish: any) {
    this.socket.emit('selectFood', dish)
  }

  public getFoodChangeOrder(): Observable<any> {
    return Observable.create((subscriber: Subscriber<any>) => {
      this.socket.on('foodDelivered', data => {
        subscriber.next(data)
      })
    })
  }
}
