import { Component, OnInit, ViewChild } from '@angular/core';
import { DishesService } from './services/dishes.service';
import {  ChartOptions, ChartType,  ChartData } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';

interface DishI {
  image: string;
  name: string;
  stock: number;
  _id: string;
  index?: number;
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild("baseChart")
    chart: BaseChartDirective;
  dishes: DishI[] = [];
  myData = [
  ];
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartOptions: ChartOptions = {
    responsive: true,
    showLines: false,
    
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }] },
   
  };
  public barCharData: number[] = [1,2,3]
 
  public chartColors: Color[] = [{
    backgroundColor: '#118FFF'
  }]

  constructor(
    private _dishService: DishesService
  ){

  };

  ngOnInit(){
    this._dishService.getAll().then(res =>  {
       this.dishes = res.data;

      const foodhandler = this._dishService.getFoodChangeOrder();

      foodhandler.subscribe({
        next: (obs) => {
          console.log("server socket emitter", obs);
          this.changefood(obs);
        }
      });


    }).finally(() => {
      this.dishes.forEach((el) => {
        this.myData.push( el.stock)
        this.barChartLabels.push(el.name)
      })
    })
  };

  selectFood(dish: DishI, index: number){
    this._dishService.deliverFood({...dish, index});
    this.dishes[index].stock = dish.stock - 1;
    this.myData[index] = dish.stock;

    if(this.chart !== undefined){
      this.chart.ngOnDestroy();
      this.chart.chart = this.chart.getChartBuilder(this.chart.ctx);
}
  }

  changefood(data): void{
    console.log(this.myData[data.index][1]);
    const index = this.dishes.findIndex(el => el._id === data._id);
    let myNewData = this.myData;
    myNewData[index] = data.stock;
    this.myData = myNewData;
    this.dishes[index] = data;
    if(this.chart !== undefined){
      this.chart.ngOnDestroy();
      this.chart.chart = this.chart.getChartBuilder(this.chart.ctx);
}
  };
}
