const { response } = require('express');

const Dish = require('../models/dish');

const createDish = async (req, res = response ) => {

	try {
		const dish = Dish(req.body);
		await dish.save();
		res.status(201).json({
			success: true,
			message: 'Dish created!',
		});
	} catch (error) {
		res.status(500).json({
			success: false,
			message: 'Ha ocurrido un error en el servidor',
			error
		});
	};
};

const getAllDishes = async (req, res = response ) => {
	try {
		const dishes = await Dish.find();
		// await dish.save();
		res.status(200).json({
			success: true,
			message: 'Dishes foundend',
			data: dishes
		});
	} catch (error) {
		res.status(500).json({
			success: false,
			message: 'Ha ocurrido un error en el servidor',
			error
		});
	}
}


module.exports = {
	createDish,
	getAllDishes
};