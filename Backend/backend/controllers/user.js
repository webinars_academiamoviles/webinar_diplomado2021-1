const { response } = require('express');
const { checkIfEmailExist } = require('../middlewares/user/user.middleware');

const User = require('../models/user');

const createUser = async (req, res = response ) => {

	try {
		const { email } = req.body;
		const verifyEmail = await checkIfEmailExist(email);
		if(verifyEmail){
			console.log("email founded!")
			res.status(verifyEmail.status).json({
				success: false,
				message: verifyEmail.message,
				error: verifyEmail.error
			});
			return;
		};
		const user = User(req.body);
		await user.save();
		res.json({
			success: true,
			message: 'User created!',
		});
	} catch (error) {
		res.status(500).json({
			success: false,
			message: 'Ha ocurrido un error en el servidor',
			error
		});
	};
};


module.exports = {
	createUser
};