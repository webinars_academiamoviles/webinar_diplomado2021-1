const User = require("../../models/user")

const checkIfEmailExist = async (requestEmail) => {
	try {
		const email = await User.findOne({email: requestEmail});
		if(email){
			return {
				status: 400,
				message: 'Email exist',
				error: email
			};
		};
		return null;
	} catch (error) {
		return {
			status: 500,
			message: 'Ha ocurrido un error interno',
			error
		}
	}
};

module.exports = {
	checkIfEmailExist
};