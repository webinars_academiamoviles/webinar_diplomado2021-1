const Dish = require("../../models/dish");

const deliverFood = async (food) => {
	try {
		console.log("food: ", food);
		const dish = await Dish.findOne({_id: food._id});
		if(dish && dish.stock > 0){
			const stock = dish.stock  - 1
			await Dish.updateOne({_id: food._id}, {...food, stock});
			console.log("finished: ", dish);
			console.log("finished stock: ", stock);
			return {...food, stock}
		}else {
			return null
		}
		
	} catch (error) {
		console.log("error")
		return null
	}
};


module.exports = deliverFood;