const express = require('express');
const cors = require('cors');

const app = express();
require('dotenv').config();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
	cors: {
		origin: "*",
		methods: ["GET", "POST", "PUT", "PATCH"],
	  }
});

const { dbConnection } = require('./database/config');

module.exports = { io }
require('./sockets/index');

//db config

dbConnection();

app.use(cors());

app.get('/', (req, res) => {
	const body = {
		saludo: 'hello world! with sockets!'
	}
	res.send(body);
})

app.use(express.json() );

app.use('/api/user', require('./routes/user'));
app.use('/api/dish', require('./routes/dish'));

server.listen(process.env.PORT, () => {
	console.log(`The app is running on port ${process.env.PORT}`);
});

