// path: api/login/

const { Router } = require('express');
const { createUser } = require('../controllers/user');
const { check } = require('express-validator');
const { fieldValidator } = require('../middlewares/validar-campos');

const router = Router();

router.post('/new', [
	check('name', 'El campo nombre es obligatorio').not().isEmpty(),
	check('password', 'El campo contraseña es obligatorio').not().isEmpty(),
	check('email', 'El campo de email es obligatorio').isEmail(),
	fieldValidator
] ,createUser);


module.exports = router;