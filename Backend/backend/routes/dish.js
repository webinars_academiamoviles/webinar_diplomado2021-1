// path: api/login/

const { Router } = require('express');
const { createDish, getAllDishes } = require('../controllers/dish');
const { check } = require('express-validator');

const router = Router();

router.post('/new', [
	check('name', 'El campo nombre es obligatorio').not().isEmpty(),
	check('stock', 'El campo de stock es obligatorio').not().isEmpty(),
	check('image', 'El cmapo de image es obligatorio').not().isEmpty()
] , createDish);

router.get('/all',getAllDishes );



module.exports = router;