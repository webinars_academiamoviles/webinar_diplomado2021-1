const { io } = require("../app");
const deliverFood = require("../middlewares/dish/deliverFood");
const Dish = require("../models/dish");


// Mensajes de Sockets
io.on('connection', (client) =>  {

	console.log("connected: ");
	// Escuchar del cliente el mensaje-personal
	
	client.on('selectFood',async (payload) => {
		const delivered = await deliverFood(payload);
		if(delivered){
			console.log("delivered: ", delivered);
			client.broadcast.emit('foodDelivered', delivered);
		}
	});
    
    client.on('disconnect', () => {
    });




});
