const { Schema, model }  = require('mongoose');

const FoodOrderSchema = Schema ({
	plato: {
		type: Schema.Types.ObjectId,
		required: true
	},
	mesa: {
		type: Schema.Types.Number,
		required: true
	}
})