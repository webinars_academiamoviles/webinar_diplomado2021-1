const { Schema, model } = require('mongoose');

const DishSchema = Schema({
	name: {
		type: String,
		required: true
	},
	stock: {
		type: Number,
		required: true
	},
	image: {
		type: String,
		required: true
	}
});

module.exports = model('Dish', DishSchema);