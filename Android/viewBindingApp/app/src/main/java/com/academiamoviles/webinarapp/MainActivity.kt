package com.academiamoviles.webinarapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.academiamoviles.webinarapp.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //Buterknife
    //@BindView(R.id.btnIniciar)
    //lateinit var btnIniciar: Button

    //@BindView(R.id.tvPie)
    //lateinit var tvPie: TextView

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        //ButterKnife.bind(this)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvPie.text = "No te pierdas e inicia el diplomado - 06 de Febrero"

        binding.btnIniciar.setOnClickListener {
            Toast.makeText(baseContext,"Bienvenidos al diplomado 2021",Toast.LENGTH_LONG).show()
        }

        //ViewBinding
        /*ActivityMainBinding.inflate(layoutInflater).apply {
            setContentView(root)

            tvPie.text = "Inicio - 03 de Febrero"

            btnIniciar.setOnClickListener {
                Toast.makeText(baseContext,"Bienvenidos al diplomado 2021",Toast.LENGTH_LONG).show()
            }
        }*/


        //uso con findViewId
        //val btnIniciar = findViewById<Button>(R.id.btnIniciar)
        //val tvPie = findViewById<TextView>(R.id.tvMensaje)

        //tvPie.text = "Inicio - 03 de Febrero"

        //btnIniciar.setOnClickListener {
        //    Toast.makeText(this,"Bienvenidos al diplomado 2021",Toast.LENGTH_LONG).show()
        //}

        //Kotlin Synthetics  



    }
}