//
//  ProfileViewController.swift
//  Diplomado2021
//
//  Created by Kenyi Rodriguez on 3/02/21.
//  Copyright © 2021 Kenyi Rodriguez. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBAction func clickBtnShowLess(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3) {
            
            let showMore = sender.tag == 1
            sender.tag = showMore ? 0 : 1
            let buttonTitle = showMore ? "Mostrar menos" : "Mostrar más"
            
            self.lblDescription.numberOfLines = showMore ? 0 : 4
            sender.setTitle(buttonTitle, for: .normal)
            
            self.view.layoutIfNeeded()
        }
    }
}
